package com.aquasoft;

/**
 * Contains list of application's level constants declarations.
 * <p/>
 * Don't declare any method signatures here.
 *
 * @author Alexey Kovalev
 * @since 19.09.2015
 */
public interface Constants {

    String CELL_DATA_DELIMITER = "\\s+";

    String NEW_LINE_SYMBOL = System.getProperty("line.separator");

    /**
     * Constants relevant for {@code View} level
     * according to {@code MVC} structure.
     */
    interface View {
        String ARGS_NAMESPACE = "com.aquasoft.args";
        String EXTRA_SOURCE_FILE = ARGS_NAMESPACE + ".source.file";
    }
}