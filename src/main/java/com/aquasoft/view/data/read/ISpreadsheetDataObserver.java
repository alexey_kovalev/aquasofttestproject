package com.aquasoft.view.data.read;

/**
 * Intended for linking {@link ISpreadsheetDataReadStrategy}
 * particular implementation with object which are interested in
 * reading results.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public interface ISpreadsheetDataObserver {

    void onSpreadsheetHeaderReceived(String spreadsheetHeader);

    void onNextCellDataReceived(String expression);

    void onError(Exception ex);
}
