package com.aquasoft.view.data.read;

import com.aquasoft.Constants;
import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Intended for reading spreadsheet data from file based source.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public final class FileBasedSpreadsheetReadStrategy implements ISpreadsheetDataReadStrategy {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    private final List<String> fileData;
    private int currentRowInFile;

    public FileBasedSpreadsheetReadStrategy(File sourceFile) throws IOException {
        this.fileData = FileUtils.readLines(sourceFile);
        logger.debug("Read file's data -> " + fileData);
    }

    @Override
    public void readNextSpreadsheet(ISpreadsheetDataObserver observer) throws IOException {
        seekToContentLine();
        readHeader(observer);
        // read either till the end of file or to the new line (indicates that we have another spreadsheet)
        while (currentRowInFile < fileData.size() && StringUtils.isNotBlank(fileData.get(currentRowInFile))) {
            readCells(observer);
        }
    }

    /**
     * Reads spreadsheet header - its dimensions.
     */
    private void readHeader(ISpreadsheetDataObserver observer) {
        observer.onSpreadsheetHeaderReceived(fileData.get(currentRowInFile));
        ++currentRowInFile;
    }

    private void readCells(ISpreadsheetDataObserver observer) {
        Arrays.asList(fileData.get(currentRowInFile).split(Constants.CELL_DATA_DELIMITER))
                .forEach(observer::onNextCellDataReceived);
        ++currentRowInFile;
    }

    private void seekToContentLine() {
        while (currentRowInFile < fileData.size() && StringUtils.isBlank(fileData.get(currentRowInFile))) {
            ++currentRowInFile;
        }
    }

    @Override
    public boolean hasNextSpreadsheet() {
        // todo: very simple checking logic. Refine in future.
        return currentRowInFile < fileData.size();

        /*if (fileData == null || currentRowInFile >= fileData.size()) {
            return false;
        }
        List<String> fromCurrentPosTillEnd = fileData.subList(currentRowInFile, fileData.size());
        boolean isRestOfFileEmpty = CollectionUtils.matchesAll(fromCurrentPosTillEnd,
                (fileLine) -> NEW_LINE_SYMBOL.equals(fileLine) || StringUtils.isBlank(fileLine));
        return !isRestOfFileEmpty;*/
    }
}
