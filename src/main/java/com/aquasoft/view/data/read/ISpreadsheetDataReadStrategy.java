package com.aquasoft.view.data.read;

import java.io.IOException;

/**
 * This interface has to be implemented by classes which are going to
 * play role readers of source spreadsheet data.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public interface ISpreadsheetDataReadStrategy {

    void readNextSpreadsheet(ISpreadsheetDataObserver observer) throws IOException;

    boolean hasNextSpreadsheet();
}
