package com.aquasoft.view.data.read;

import com.aquasoft.Constants;
import com.aquasoft.model.exception.Exceptions;
import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Intended for reading spreadsheet data from standard input.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public final class StdInBasedSpreadsheetReadStrategy implements ISpreadsheetDataReadStrategy {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    private final Scanner consoleDataReader;

    public StdInBasedSpreadsheetReadStrategy(InputStream inputStream) {
        this.consoleDataReader = new Scanner(inputStream);
    }

    public void readNextSpreadsheet(ISpreadsheetDataObserver observer) throws IOException {
        String spreadsheetHeaderLine = consoleDataReader.nextLine();
        observer.onSpreadsheetHeaderReceived(spreadsheetHeaderLine);
        // todo: not good to do some parsing here
        String[] headerData = spreadsheetHeaderLine.split(Constants.CELL_DATA_DELIMITER);
        final int spreadsheetHeight = Integer.valueOf(headerData[0]);
        for (int i = 0; i < spreadsheetHeight; i++) {
            final String cellsDataRow = consoleDataReader.nextLine();
            readCells(cellsDataRow, observer);
        }
    }

    private void readCells(String cellsDataRow, ISpreadsheetDataObserver observer) {
        Arrays.asList(cellsDataRow.split(Constants.CELL_DATA_DELIMITER))
                .forEach(observer::onNextCellDataReceived);
    }

    @Override
    public boolean hasNextSpreadsheet() {
        return Exceptions.unsupportedOperation("hasNextSpreadsheet() is not supported " +
                "for StdInBasedSpreadsheetReadStrategy strategy");
    }
}
