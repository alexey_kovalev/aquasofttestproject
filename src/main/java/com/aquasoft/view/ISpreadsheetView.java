package com.aquasoft.view;

import com.aquasoft.model.entity.SpreadsheetComputationResultWrapper;

/**
 * This interface should be implemented by classes
 * which are going to play role of {@code View}
 * according to {@code MVC} architecture.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public interface ISpreadsheetView {

    void onResult(SpreadsheetComputationResultWrapper result);

    void onError(String message, Exception cause);
}
