package com.aquasoft.view;

import com.aquasoft.Constants;
import com.aquasoft.controller.ISpreadsheetController;
import com.aquasoft.model.entity.SpreadsheetComputationResultWrapper;
import com.aquasoft.model.entity.cell.ComputationResult;
import com.aquasoft.model.util.CellUtils;
import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;
import com.aquasoft.view.control.ControlCommand;
import com.aquasoft.view.control.ControlCommandsInterceptor;
import com.aquasoft.view.data.read.*;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @see ISpreadsheetView
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public final class ConsoleSpreadsheetView implements ISpreadsheetView {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    private final ISpreadsheetController spreadsheetController;

    private final ControlCommandsInterceptor controlCommandsInterceptor;

    private ISpreadsheetDataObserver spreadsheetSourceDataReceiver = new ISpreadsheetDataObserver() {
        @Override
        public void onSpreadsheetHeaderReceived(String spreadsheetHeader) {
            spreadsheetController.onSpreadsheetHeaderReceived(spreadsheetHeader);
        }

        @Override
        public void onNextCellDataReceived(String expression) {
            spreadsheetController.onNextCellDataReceived(expression);
        }

        @Override
        public void onError(Exception ex) {
            logger.error(ex);
        }
    };

    public ConsoleSpreadsheetView(ISpreadsheetController spreadsheetController) {
        this.spreadsheetController = spreadsheetController;
        this.controlCommandsInterceptor = new ControlCommandsInterceptor();
    }

    @Override
    public void onResult(SpreadsheetComputationResultWrapper result) {
        System.out.println("\nCalculation result:");
        int rows = result.getSpreadsheetHeight();
        int columns = result.getSpreadsheetLength();

        String[][] matrixToPrint = new String[rows][columns];

        // iterate over each row and get computation result for each cell
        IntStream.range(0, rows)
                .forEach(rowIndex -> {
                    List<String> rowResult =
                            CellUtils.getCellsComputationResultByRow(rowIndex + 1, result.getResultMap())
                                    .stream()
                                    .map(ComputationResult::getResult)
                                    .collect(Collectors.toList());
                    matrixToPrint[rowIndex] = rowResult.toArray(new String[rowResult.size()]);
                });

        PrettyPrinter prettyPrinter = new PrettyPrinter(System.out);
        prettyPrinter.print(matrixToPrint);
    }

    @Override
    public void onError(String message, Exception cause) {
        String resMessage = message + (cause != null ? cause.toString() : StringUtils.EMPTY);
        System.out.println("ERROR: " + resMessage);
    }

    public void interceptStdInput(String[] inputArgs) throws IOException {
        System.out.println("Welcome to spreadsheet processor.");
        final Properties parsedArgs = parseArgs(inputArgs);
        ReadStrategyHolder readStrategyChooseRes = chooseSpreadsheetReadStrategy(parsedArgs);

        // todo: not very good check - polymorphism lost. Think about better solution.
        if (readStrategyChooseRes.strategyType == ReadStrategyHolder.StrategyType.FileBased) {
            oneShotFileRead(readStrategyChooseRes.strategy);
        } else if (readStrategyChooseRes.strategyType == ReadStrategyHolder.StrategyType.StdInBased) {
            consoleRead(readStrategyChooseRes.strategy);
        } else {
            throw new RuntimeException("Implementation error. Should not never happen.");
        }
    }

    private void oneShotFileRead(ISpreadsheetDataReadStrategy fileReadStrategy) throws IOException {
        while (fileReadStrategy.hasNextSpreadsheet()) {
            fileReadStrategy.readNextSpreadsheet(spreadsheetSourceDataReceiver);
        }
    }

    private void consoleRead(ISpreadsheetDataReadStrategy consoleReadStrategy) throws IOException {
        printConsoleReadHeader();
        Scanner consoleDataReader = new Scanner(System.in);
        while (consoleDataReader.hasNextLine()) {
            String line = consoleDataReader.nextLine();
            final ControlCommand controlCommand = ControlCommand.parse(line);
            if (controlCommandsInterceptor.isControlCommand(controlCommand)) {
                if (controlCommand.getType() == ControlCommand.Type.NewSpreadsheet) {
                    System.out.println("Please enter spreadsheet data starting from header:");
                    // read spreadsheet data from console input
                    // delegate call to StdInBased spreadsheet reader
                    consoleReadStrategy.readNextSpreadsheet(spreadsheetSourceDataReceiver);
                } else {
                    controlCommandsInterceptor.executeCommand(controlCommand);
                }
            } else {
                System.out.println(String.format("Unrecognized command \"%s\". Please check syntax.", line));
            }
        }
    }

    private void printConsoleReadHeader() {
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("1) For entering spreadsheet data please print out \"--new_spreadsheet\" (without quotation mark)");
        System.out.println("2) For exit please print out \"--exit\" command (without quotation mark)");
        System.out.println("---------------------------------------------------------------------------");
    }

    /**
     * Chooses particular implementation of {@link ISpreadsheetDataReadStrategy}
     * depending on passed arguments.
     */
    private ReadStrategyHolder chooseSpreadsheetReadStrategy(Properties parsedArgs) throws IOException {
        logger.debug("chooseSpreadsheetReadStrategy()");
        if (parsedArgs.get(Constants.View.EXTRA_SOURCE_FILE) != null) {
            String fileName = (String) parsedArgs.get(Constants.View.EXTRA_SOURCE_FILE);
            logger.debug("get file by name [" + fileName + "]");
            if (!fileName.isEmpty()) {
                ClassLoader classLoader = getClass().getClassLoader();
                File sourceFile = new File(classLoader.getResource(fileName).getFile());
                if (sourceFile.exists()) {
                    logger.debug("chosen FileBasedSourceReadStrategy");
                    return new ReadStrategyHolder(
                            ReadStrategyHolder.StrategyType.FileBased,
                            new FileBasedSpreadsheetReadStrategy(sourceFile)
                    );
                }
            }
        }

        logger.debug("chosen StdInBasedSourceReadStrategy");
        return new ReadStrategyHolder(
                ReadStrategyHolder.StrategyType.StdInBased,
                new StdInBasedSpreadsheetReadStrategy(System.in)
        );
    }

    /**
     * Parses arguments list passed as additional params during launching
     * application.
     */
    private Properties parseArgs(String[] inputArgs) {
        final Properties parsedArgs = new Properties();
        if (inputArgs == null || inputArgs.length == 0) {
            return parsedArgs;
        }
        final Pattern sourceFilePattern = Pattern.compile("^--file='([a-zA-Z0-9_.-]+)'$");
        for (String arg : inputArgs) {
            final Matcher fileMatcher = sourceFilePattern.matcher(arg);
            if (fileMatcher.matches()) {
                String fileName = fileMatcher.group(1);
                parsedArgs.put(Constants.View.EXTRA_SOURCE_FILE, fileName);
            }
        }
        return parsedArgs;
    }

    /**
     * Simple holder which stores result of choosing spreadsheet reading
     * strategy.
     */
    private static final class ReadStrategyHolder {
        enum StrategyType {FileBased, StdInBased}

        final StrategyType strategyType;
        final ISpreadsheetDataReadStrategy strategy;

        ReadStrategyHolder(StrategyType strategyType, ISpreadsheetDataReadStrategy strategy) {
            this.strategyType = strategyType;
            this.strategy = strategy;
        }
    }
}
