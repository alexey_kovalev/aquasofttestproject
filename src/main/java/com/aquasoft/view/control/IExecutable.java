package com.aquasoft.view.control;

/**
 * Functional interface for executing action defined by
 * {@link #execute()} method
 *
 * @author Alexey Kovalev
 * @since 27.08.15
 */
interface IExecutable {

    void execute();
}
