package com.aquasoft.view.control;

import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;

/**
 * Intended for intercepting and execution control commands
 * that user might enter in console.
 * Particular example of such command is {@code exit} or something.
 *
 * @author Alexey Kovalev
 * @since 27.08.15
 */
public final class ControlCommandsInterceptor {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    public boolean isControlCommand(ControlCommand controlCommand) {
        return controlCommand != ControlCommand.NOT_CONTROL_COMMAND;
    }

    public void executeCommand(ControlCommand controlCommand) {
        logger.debug("executeCommand [" + controlCommand + "]");
        switch (controlCommand.getType()) {
            case Exit:
                logger.debug("Execute EXIT operation");
                execute(() -> System.exit(0));
                break;
            default:
        }
    }

    private void execute(IExecutable executable) {
        executable.execute();
    }
}
