package com.aquasoft.view.control;

import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents an action which might be performed
 * as an response to user's input in case when this
 * input was valid from the perspective of {@link ControlCommand#parse(String)}
 * method.
 * <p/>
 * Examples of such commands (actions) might be {@code exit} command which
 * interrupts application, {@code new_spreadsheet} command which allows user
 * to enter spreadsheet source data.
 *
 * @author Alexey Kovalev
 * @since 27.08.15
 */
public final class ControlCommand {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    private static final Map<String, Object> EMPTY_PARAMS = new HashMap<>();
    public static final ControlCommand NOT_CONTROL_COMMAND = new ControlCommand(Type.NotDefined, EMPTY_PARAMS);

    private static final String CONTROL_COMMAND_REGEX = "^--(\\w+)$";
    private static final Pattern CONTROL_COMMAND_PATTERN = Pattern.compile(CONTROL_COMMAND_REGEX);

    public enum Type {

        Exit("exit"),
        NewSpreadsheet("new_spreadsheet"),
        NotDefined("not_defined");

        private final String asString;

        Type(String asString) {
            this.asString = asString;
        }

        static Type fromString(String asString) {
            for (Type type : Type.values()) {
                if (type.asString.equals(asString)) {
                    return type;
                }
            }
            return NotDefined;
        }

        @Override
        public String toString() {
            return asString;
        }
    }

    private final Type type;
    private final Map<String, Object> params;

    private ControlCommand(Type type, Map<String, Object> params) {
        this.type = type;
        this.params = params;
    }

    public static ControlCommand parse(String commandToParse) {
        logger.debug("Parse command [" + commandToParse + "]");
        final Matcher commandMatcher = CONTROL_COMMAND_PATTERN.matcher(commandToParse);
        if (commandMatcher.matches()) {
            final ControlCommand controlCommand = toControlCommand(commandMatcher.group(1), StringUtils.EMPTY);
            logger.debug("Control command detected. Command -> " + controlCommand);
            return controlCommand;
        } else {
            logger.debug("Is NOT control command.");
            return NOT_CONTROL_COMMAND;
        }
    }

    private static ControlCommand toControlCommand(String command, String params) {
        final Type commandType = Type.fromString(command);
        return commandType == Type.NotDefined ?
                NOT_CONTROL_COMMAND :
                new ControlCommand(commandType, readCommandParams(params));
    }

    private static Map<String, Object> readCommandParams(String params) {
        // todo: for now simple stub. In future parsing of passed additional params
        return EMPTY_PARAMS;
    }

    public Type getType() {
        return type;
    }

    public Map<String, Object> getParams() {
        return Collections.unmodifiableMap(params);
    }

    @Override
    public String toString() {
        return "ControlCommand{" +
                "type=" + type +
                ", params=" + params +
                '}';
    }
}
