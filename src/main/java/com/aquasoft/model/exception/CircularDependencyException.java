package com.aquasoft.model.exception;

import com.aquasoft.model.entity.cell.Cell;
import com.aquasoft.model.entity.cell.CellCoordinate;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author Alexey Kovalev
 * @since 17.09.2015.
 */
public class CircularDependencyException extends Exception {

    public CircularDependencyException() {
        super();
    }

    public CircularDependencyException(String message, Iterable<Cell> circularyDepentantCells) {
        this(message, circularyDepentantCells, null);
    }

    public CircularDependencyException(String message, Iterable<Cell> circularyDepentantCells, Throwable cause) {
        super(wrapMessage(message, circularyDepentantCells), cause);
    }

    private static String wrapMessage(String message, Iterable<Cell> circularyDepentantCells) {
        return message + "{" + getCircularryDependentCellsAsString(circularyDepentantCells) + "}";
    }

    private static String getCircularryDependentCellsAsString(Iterable<Cell> circularyDepentantCells) {
        Set<CellCoordinate> res = CollectionUtils.collect(circularyDepentantCells, Cell::getCoordinate, new TreeSet<>());
        return res.toString();
    }
}
