package com.aquasoft.model.exception;

/**
 * Utility class for assisting exceptional conditions.
 * <p/>
 * You can't directly instantiate this class and have
 * to use one of its static methods.
 *
 * @author Alexey Kovalev
 * @since 23.08.2015
 */
public class Exceptions {

    /**
     * Not instances.
     */
    private Exceptions() {
        throw new AssertionError("No instances of Exceptions class, use static methods instead.");
    }

    /**
     * Invoke it in case when you are going to throw {@link UnsupportedOperationException}
     * for some not implemented feature yet.
     *
     * @return dummy instance of {@code T} type
     * @throws UnsupportedOperationException with specified custom message
     *         as a result of invokation
     */
    public static <T> T notImplementedFeatureYet(String message) {
        throw new UnsupportedOperationException(message);
    }

    /**
     * One of the flavor of {@link #notImplementedFeatureYet(String)}
     * with message "{@code This feature has not been implemented yet.} "
     *
     * @see #notImplementedFeatureYet(String)
     */
    public static <T> T notImplementedFeatureYet() {
        return notImplementedFeatureYet("This feature has not been implemented yet.");
    }

    public static <T> T unsupportedOperation(String message) {
        throw new UnsupportedOperationException(message);
    }

    /**
     * Throws {@link NullPointerException} with specified custom
     * message {@code message} in case when {@code objectToCheck} parameter
     * refers to {@code null}
     */
    public static void throwIfNull(Object objectToCheck, String message) {
        if (objectToCheck == null) {
            throw new NullPointerException(message);
        }
    }

}
