package com.aquasoft.model.exception;

/**
 * Thrown when some exception occurs during syntax parsing phase.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public class SyntaxException extends Exception {
    public SyntaxException() {
        super();
    }

    public SyntaxException(String message) {
        super(message);
    }

    public SyntaxException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyntaxException(Throwable cause) {
        super(cause);
    }
}
