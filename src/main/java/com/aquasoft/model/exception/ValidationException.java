package com.aquasoft.model.exception;

/**
 * Thrown in case when one of the validation condition is violated.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public class ValidationException extends Exception {

    public ValidationException() {
        super();
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }
}
