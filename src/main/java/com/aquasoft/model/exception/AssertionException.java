package com.aquasoft.model.exception;

/**
 * Thrown to indicate that some particular assertion condition
 * has not been satisfied - i.e. for failed assertion statements.
 *
 * @author Alexey Kovalev
 * @since 23.08.2015
 */
public class AssertionException extends RuntimeException {

    public AssertionException() {
    }

    public AssertionException(String message) {
        super(message);
    }

    public AssertionException(String message, Throwable cause) {
        super(message, cause);
    }

    public AssertionException(Throwable cause) {
        super(cause);
    }
}
