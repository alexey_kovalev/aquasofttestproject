package com.aquasoft.model.exception;

/**
 * Exceptional condition which might occur during {@link com.aquasoft.model.entity.token.AbstractExpressionToken}
 * concrete instance creation.
 *
 * @author Alexey Kovalev
 * @since 08.09.2015
 */
public class TokenCreationException extends Exception {

    public TokenCreationException() {
        super();
    }

    public TokenCreationException(String message) {
        super(message);
    }

    public TokenCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenCreationException(Throwable cause) {
        super(cause);
    }
}
