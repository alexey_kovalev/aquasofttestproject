package com.aquasoft.model.processor;

import com.aquasoft.model.entity.Spreadsheet;
import com.aquasoft.model.entity.cell.Cell;
import com.aquasoft.model.entity.cell.CellCoordinate;
import com.aquasoft.model.exception.CircularDependencyException;
import com.aquasoft.model.util.TimeAndDateUtils;
import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

/**
 * Represents a central point for cell's expression computation activities.
 * The main flow of work:
 * <ol>
 *     <li>Create dependencies graph. At this stage all passed cells will be separated onto
 *     computation layers - chunks of calculation (see {@link ComputationChunk})
 *     </li>
 *     <br/>
 *     <li>Compute resulting chunks starting from the bottom. So this means
 *     that graph's evaluation will start from the layer which depends on nothing then computation
 *     will move towards up computing cells (also contained in layers) which depends on underlying
 *     ones and so on
 *     </li>
 * </ol>
 * <p/>
 *
 * In order to use processor create an instance of it using predefined static factory passing
 * {@link Spreadsheet} instance as parameter and invoke {@link #evaluate()} which returns
 * resulting map containing result of computations.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public final class CellsProcessor {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    /**
     * Queue of computation chunks which subsequently will be calculated
     * starting from the queue's head (because this chunk does not depend on others).
     */
    private final Queue<ComputationChunk> computationChunks;

    /**
     * Contains a list of cell's coordinates for which dependencies has been
     * already established.
     */
    private final Set<CellCoordinate> alreadyEvaluatedCoordinates;

    /**
     * Set of all cells which contains in evaluating source spreadsheet.
     */
    private final Set<Cell> allCells;

    /**
     * Dictionary which establish correspondence between {@link CellCoordinate}
     * and {@link Cell} for already calculated cells.
     */
    private final Map<CellCoordinate, Cell> computedDependenciesPool;

    /**
     * No outside instantiation. Use provided static factories instead.
     */
    private CellsProcessor(Set<Cell> content) {
        this.computationChunks = new LinkedList<>();
        // create safe copy of all cells
        this.allCells = new HashSet<>(content);
        this.alreadyEvaluatedCoordinates = new HashSet<>();
        this.computedDependenciesPool = new HashMap<>();
    }

    /**
     * Creates instance of {@link CellsProcessor} from supplied
     * {@link Spreadsheet} instance.
     */
    public static CellsProcessor from(Spreadsheet spreadsheet) {
        return new CellsProcessor(spreadsheet.getCells());
    }

    /**
     * Entry point for starting spreadsheet calculation.
     *
     * @throws CircularDependencyException - in case when circular dependency between some cells detected
     */
    public Map<CellCoordinate, Cell> evaluate() throws CircularDependencyException {
        buildDependencies();
        calculate();
        return Collections.unmodifiableMap(new TreeMap<>(computedDependenciesPool));
    }

    /**
     * Calculates accumulated queue of computation chunks.
     */
    private void calculate() {
        final long startTime = TimeAndDateUtils.nowTimeInMillis();
        for (ComputationChunk chunk : computationChunks) {
            chunk.calculate(computedDependenciesPool);
            computedDependenciesPool.putAll(chunk.toMap());
        }
        logger.debug("Graph calculation finished. Took time [" + (TimeAndDateUtils.nowTimeInMillis() - startTime) + "] ms.");
    }

    /**
     * Creates computation chunks queue, taking into account dependencies (references)
     * between spreadsheet cells.
     *
     * @throws CircularDependencyException - in case when circular dependency between some cells detected
     */
    private void buildDependencies() throws CircularDependencyException {
        final long startTime = TimeAndDateUtils.nowTimeInMillis();
        ComputationChunk chunk;
        while (!(chunk = createChunk()).isEmpty()) {
            computationChunks.offer(chunk);
        }

        // todo: very rough check refine in future
        if (!allCells.isEmpty()) {
            throw new CircularDependencyException("Circular dependencies for cells.", allCells);
        }

        logger.debug("Building dependencies finished. Took time [" + (TimeAndDateUtils.nowTimeInMillis() - startTime) + "] ms");
    }

    private ComputationChunk createChunk() {
        ComputationChunk res = new ComputationChunk();
        Iterator<Cell> allCellsIterator = allCells.iterator();
        Set<CellCoordinate> locallyEvaluated = new HashSet<>();
        while (allCellsIterator.hasNext()) {
            Cell cell = allCellsIterator.next();
            if (cell.isDependenciesFree() || isAllDependenciesSatisfied(cell)) {
                res.add(cell);
                locallyEvaluated.add(cell.getCoordinate());
                allCellsIterator.remove();
            }
        }
        alreadyEvaluatedCoordinates.addAll(locallyEvaluated);
        return res;
    }

    private boolean isAllDependenciesSatisfied(Cell cellToCheck) {
        return CollectionUtils.containsAll(alreadyEvaluatedCoordinates, cellToCheck.getDependencies());
    }
}
