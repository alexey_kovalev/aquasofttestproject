package com.aquasoft.model.processor;

import com.aquasoft.model.entity.cell.Cell;
import com.aquasoft.model.entity.cell.CellCoordinate;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Contains a set of cells which lays at the same "dependency level".
 * I.e. this cells don't depend on each other, but they might depend
 * on other cells which lays at underlying layers (other {@link ComputationChunk})
 *
 * @author Alexey Kovalev
 * @since 14.09.2015
 */
final class ComputationChunk {

    private final Set<Cell> inhabitants;

    public ComputationChunk() {
        inhabitants = new HashSet<>();
    }

    public void add(Cell newInhabitant) {
        inhabitants.add(newInhabitant);
    }

    public void addMany(Cell... newInhabitants) {
        inhabitants.addAll(Arrays.asList(newInhabitants));
    }

    public void addMany(Collection<Cell> newInhabitants) {
        inhabitants.addAll(newInhabitants);
    }

    public boolean containsAllCells(Collection<Cell> candidatesToCheck) {
        return CollectionUtils.containsAll(inhabitants, candidatesToCheck);
    }

    public Set<Cell> getInhabitants() {
        return Collections.unmodifiableSet(inhabitants);
    }

    public boolean isEmpty() {
        return inhabitants.size() == 0;
    }

    public void calculate(Map<CellCoordinate, Cell> computedDependenciesPool) {
        inhabitants.forEach((cell) -> cell.calculate(computedDependenciesPool));
    }

    public Map<CellCoordinate, Cell> toMap() {
        return Collections.unmodifiableMap(
                inhabitants
                    .stream()
                    .collect(Collectors.toMap(Cell::getCoordinate, (cell) -> cell))
        );
    }
}
