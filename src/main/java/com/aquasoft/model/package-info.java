/**
 * According to {@code MVC} pattern this package will contain
 * entities related to {@code Model} layer.
 */
package com.aquasoft.model;