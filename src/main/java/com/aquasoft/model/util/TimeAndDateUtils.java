package com.aquasoft.model.util;

/**
 * Utility class containing list of convenient methods
 * intended to assisting time and data transformation
 * facilities.
 *
 * @author Alexey Kovalev
 * @since 19.09.2015
 */
public class TimeAndDateUtils {

    /**
     * No instances.
     */
    private TimeAndDateUtils() {
        throw new AssertionError("No instances of TimeAndDateUtils");
    }

    public static long nowTimeInMillis() {
        return System.currentTimeMillis();
    }
}
