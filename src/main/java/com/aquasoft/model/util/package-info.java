/**
 * As usual this package contains miscellaneous
 * functionality that did not find shelter in other
 * parts of the system.
 */
package com.aquasoft.model.util;