package com.aquasoft.model.util;

import com.aquasoft.model.entity.BinaryOperation;
import com.aquasoft.model.entity.CellCoordinateGenerator;
import com.aquasoft.model.entity.cell.Cell;
import com.aquasoft.model.entity.cell.CellCoordinate;
import com.aquasoft.model.entity.cell.ComputationResult;
import com.aquasoft.model.entity.token.AbstractExpressionToken;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Contains convenient util methods for working with cells.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public class CellUtils {

    private static final int ASCII_A_LETTER_POSITION = 65;

    private CellUtils() {
        throw new AssertionError("No instances of CellUtils. Use static methods instead.");
    }

    /**
     * Converts parsed infix expression to corresponding postfix expression.
     */
    public static List<AbstractExpressionToken<?>> infixToPostfixNotation(List<AbstractExpressionToken<?>> parsedInfixExpression) {
        List<AbstractExpressionToken<?>> res = new ArrayList<>();
        Deque<AbstractExpressionToken<?>> stack = new ArrayDeque<>();
        for (AbstractExpressionToken<?> token : parsedInfixExpression) {
            if (token.isOperator()) {
                while (!stack.isEmpty() && hasMorePriority(stack.getFirst().castTo(BinaryOperation.class), token.castTo(BinaryOperation.class))) {
                    res.add(stack.pop());
                }
                stack.push(token);
            } else {
                res.add(token);
            }
        }

        while (!stack.isEmpty()) {
            res.add(stack.pop());
        }

        return res;
    }

    /**
     * For now operators have equal priority. Refine it if required more
     * complex logic in future.
     */
    private static boolean hasMorePriority(AbstractExpressionToken<BinaryOperation> first, AbstractExpressionToken<BinaryOperation> second) {
        // operators has same priority
        return true;
    }

    public static int getColumnIndexForLetter(char letter) {
        int shift = (int) letter - ASCII_A_LETTER_POSITION;
        return shift + CellCoordinateGenerator.START_COLUMN_INDEX;
    }

    public static List<ComputationResult> getCellsComputationResultByRow(int rowIndex, Map<CellCoordinate, Cell> cells) {
        Assertions.assertStrictPositive(rowIndex);
        return cells.entrySet()
                .stream()
                .filter(entry -> entry.getKey().getRowIndex() == rowIndex)
                .map(entry -> entry.getValue().getComputationResult())
                .collect(Collectors.toList());
    }
}
