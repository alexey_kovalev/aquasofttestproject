package com.aquasoft.model.util.log;

/**
 * Defines a general contract which concrete logger implementations
 * should adhere.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public interface ILogger {

    enum LogLevel {

        Debug(0, "DEBUG: "), Info(1, "INFO: "), Error(2, "ERROR: ");

        private final int priority;
        private final String logPrefix;

        LogLevel(int priority, String logPrefix) {
            this.priority = priority;
            this.logPrefix = logPrefix;
        }

        public int getPriority() {
            return priority;
        }

        public String getLogPrefix() {
            return logPrefix;
        }
    }

    void setMinLogLevel(LogLevel logLevel);

    LogLevel getMinLogLevel();

    void setPrefixingEnabled(boolean isPrefixing);

    void debug(String message);

    void info(String message);

    void error(String message);

    void error(Exception ex);

    void error(String message, Exception ex);
}
