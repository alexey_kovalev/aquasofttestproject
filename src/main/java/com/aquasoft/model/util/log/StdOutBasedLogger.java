package com.aquasoft.model.util.log;

import org.apache.commons.lang3.StringUtils;

import static java.lang.System.out;

/**
 * Logger implementation which outputs data to
 * standard output {@link System#out}
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
final class StdOutBasedLogger implements ILogger {

    private LogLevel minLogLevel = LogLevel.Debug;
    private boolean isPrefixing;

    StdOutBasedLogger() {
        this.isPrefixing = true;
    }

    @Override
    public void setMinLogLevel(LogLevel logLevel) {
        minLogLevel = logLevel;
    }

    @Override
    public LogLevel getMinLogLevel() {
        return minLogLevel;
    }

    @Override
    public void setPrefixingEnabled(boolean isPrefixing) {
        this.isPrefixing = isPrefixing;
    }

    @Override
    public void debug(String message) {
        if (isLogLevelAllowed(LogLevel.Debug)) {
            out.println(getLogPrefix(LogLevel.Debug) + message);
        }
    }

    @Override
    public void info(String message) {
        if (isLogLevelAllowed(LogLevel.Info)) {
            out.println(getLogPrefix(LogLevel.Info) + message);
        }
    }

    @Override
    public void error(String message) {
        error(message, null);
    }

    @Override
    public void error(Exception ex) {
        error(StringUtils.EMPTY, ex);
    }

    @Override
    public void error(String message, Exception ex) {
        if (isLogLevelAllowed(LogLevel.Error)) {
            if (ex != null) {
                out.println(getLogPrefix(LogLevel.Error) + message + ". Caused " + ex);
            } else {
                out.println(getLogPrefix(LogLevel.Error) + message);
            }
        }
    }

    private boolean isLogLevelAllowed(LogLevel requestedLogLevel) {
        return requestedLogLevel.getPriority() >= minLogLevel.getPriority();
    }

    private String getLogPrefix(LogLevel logLevel) {
        return isPrefixing && logLevel != null ? logLevel.getLogPrefix() : StringUtils.EMPTY;
    }
}
