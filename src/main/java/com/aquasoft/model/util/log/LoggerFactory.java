package com.aquasoft.model.util.log;

/**
 * Contains set of factory methods intended for
 * required logger creation.
 * // todo: replace with Log4j | Slf4j or something
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public final class LoggerFactory {

    private static final LoggerFactory INSTANCE = new LoggerFactory();

    private final StdOutBasedLogger stdOutBasedLogger;

    private LoggerFactory() {
        this.stdOutBasedLogger = new StdOutBasedLogger();
    }

    public static ILogger getStdOutBasedLogger() {
        return INSTANCE.stdOutBasedLogger;
    }
}
