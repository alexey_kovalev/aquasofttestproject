package com.aquasoft.model.util;

import com.aquasoft.model.exception.AssertionException;

/**
 * Contains diversity of different assertion statements.
 *
 * @author Alexey Kovalev
 * @since 23.08.2015
 */
public class Assertions {

    private static final String IS_NOT_MET_PREFIX = "Assertion statement is not met. ";

    private Assertions() {
        throw new AssertionError("No instances of Assertions class. Use static methods instead.");
    }

    /**
     * Checks that passed {@code valueToCheck} parameter is strictly positive
     * ({@code valueToCheck > 0} ), otherwise throws {@link AssertionException}
     *
     * @throws AssertionException in case when specified parameter
     *         {@code valueToCheck} is not strictly positive
     */
    public static void assertStrictPositive(int valueToCheck) {
        throwIfFalse(valueToCheck > 0, String.format("Expected [%s] to be strictly positive, " +
                "but actually it does not.", valueToCheck));
    }

    private static void throwIfTrue(boolean conditionToCheck, String message) {
        if (conditionToCheck) {
            throw new AssertionException(IS_NOT_MET_PREFIX + message);
        }
    }

    private static void throwIfFalse(boolean conditionToCheck, String message) {
        throwIfTrue(!conditionToCheck, message);
    }
}
