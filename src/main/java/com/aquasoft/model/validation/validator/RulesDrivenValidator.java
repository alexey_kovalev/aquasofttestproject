package com.aquasoft.model.validation.validator;

import com.aquasoft.model.exception.Exceptions;
import com.aquasoft.model.exception.ValidationException;

import java.util.List;

/**
 * {@link AbstractValidator} implementation based on set of validation rules.
 * When {@link #validate(T)} will be invoked this implementation
 * goes through each validation rule and checks that it is not violated.
 *
 * @since 24.08.2015
 * @author Alexey Kovalev
 */
final class RulesDrivenValidator<T> extends AbstractValidator<T> {

    private final List<? extends IValidationRule<T>> validationRules;

    RulesDrivenValidator(List<? extends IValidationRule<T>> validationRules) {
        this.validationRules = validationRules;
    }

    @Override
    public void validate(T typeToValidate) throws ValidationException {
        Exceptions.throwIfNull(typeToValidate, "You have to specify NOT NULL typeToValidate parameter " +
                "for performing validation.");
        for (IValidationRule<T> rule : validationRules) {
            rule.validate(typeToValidate);
        }
    }
}
