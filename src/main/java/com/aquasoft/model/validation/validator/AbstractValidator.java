package com.aquasoft.model.validation.validator;

import com.aquasoft.model.exception.Exceptions;

import java.util.List;

/**
 * General implementation of {@link IValidator}
 * which provides convenient factory methods for
 * creating particular {@link AbstractValidator} implementations.
 *
 * @param <T> type to validate
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public abstract class AbstractValidator<T> implements IValidator<T> {

    /**
     * Not for outside instantiation (available only for subclasses).
     * Use factory methods instead.
     */
    protected AbstractValidator() {
    }

    /**
     * Static factory method which creates {@link AbstractValidator} instance
     * based on provided validation rules.
     * @see RulesDrivenValidator
     */
    public static <T> AbstractValidator<T> withRules(List<? extends IValidationRule<T>> validationRules) {
        Exceptions.throwIfNull(validationRules, "You have to specify NOT NULL validationRules parameter.");
        return new RulesDrivenValidator<>(validationRules);
    }
}
