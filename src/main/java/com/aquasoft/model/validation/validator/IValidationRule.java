package com.aquasoft.model.validation.validator;

/**
 * Defines a validation rule which will be checked upon
 * {@code T} instance and throws {@link com.aquasoft.model.exception.ValidationException}
 * in case when the object does not pass the check established by this rule.
 *
 * @see IValidator
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public interface IValidationRule<T> extends IValidator<T> {
}
