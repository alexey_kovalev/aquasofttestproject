package com.aquasoft.model.validation.validator;

import com.aquasoft.model.exception.ValidationException;

/**
 * Validator for {@code T} instance.
 * <p/>
 * This functional interface defines validation which will be done
 * upon validated object and throws {@link ValidationException}
 * in case of unsuccessful validation or violation of some validation rules.
 *
 * @param <T> - type to validate
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public interface IValidator<T> {

    void validate(T typeToValidate) throws ValidationException;
}
