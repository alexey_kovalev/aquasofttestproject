/**
 * Contains cell level validation rules relevant for
 * rules driven implementations of {@link com.aquasoft.model.validation.validator.AbstractValidator}
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
package com.aquasoft.model.validation.cell.rules;