package com.aquasoft.model.validation.cell;

import com.aquasoft.model.validation.validator.IValidationRule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Provides convenient way of accumulating validation
 * rules which will be applied upon validate instance
 * of type {@code T}
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public final class ValidationRulesHolder<T> {

    private final List<IValidationRule<T>> validationRules;

    /**
     * No outside instantiation.
     * Use {@link #builder()} for instances creation.
     */
    private ValidationRulesHolder(List<IValidationRule<T>> rules) {
        this.validationRules = rules;
    }

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    /**
     * Simplified way of creating validation rules holder without any rule inside.
     * <p/>
     * Might be useful for unit testing.
     */
    public static <T> ValidationRulesHolder<T> create() {
        return ValidationRulesHolder.<T>builder().build();
    }

    public List<IValidationRule<T>> rulesToList() {
        return Collections.unmodifiableList(validationRules);
    }

    public static final class Builder<T> {
        private final List<IValidationRule<T>> rules;

        private Builder() {
            this.rules = new ArrayList<>();
        }

        public Builder<T> addRule(IValidationRule<T> rule) {
            rules.add(rule);
            return this;
        }

        public ValidationRulesHolder<T> build() {
            return new ValidationRulesHolder<>(rules);
        }
    }
}
