package com.aquasoft.model.validation;

import com.aquasoft.model.entity.token.OperatorExpressionToken;
import com.aquasoft.model.exception.ValidationException;
import com.aquasoft.model.validation.validator.IValidationRule;
import org.apache.commons.lang3.StringUtils;

/**
 * Utility class. Plays role of holder for validation rules
 * which might be performed upon source cell's expression.
 *
 * @author Alexey Kovalev
 * @since 08.09.2015
 */
public class SourceExpressionValidationRules {

    private SourceExpressionValidationRules() {
        throw new AssertionError("No instances");
    }

    public static IValidationRule<String> notEmptyExpression() {
        return s -> {
            if (StringUtils.isBlank(s)) {
                throw new ValidationException("Source cell expression has to be not blank. " +
                        "But actually received value is [" + s + "]");
            }
        };
    }

    public static IValidationRule<String> noOperatorSignAtBeginning() {
        return s -> {
            Character firstChar = s.charAt(0);
            if (OperatorExpressionToken.isOperatorSign(firstChar)) {
                throw new ValidationException("On first expression position is prohibited to have operator sign. " +
                        "But actually was found [" + firstChar + "] operator sign.");
            }
        };
    }

    public static IValidationRule<String> noOperatorSignAtTheEnd() {
        return s -> {
            Character lastChar = s.charAt(s.length() - 1);
            if (OperatorExpressionToken.isOperatorSign(lastChar)) {
                throw new ValidationException("On last expression position is prohibited to have operator sign. " +
                        "But actually was found [" + lastChar + "] operator sign.");
            }
        };
    }
}
