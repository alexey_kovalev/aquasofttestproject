package com.aquasoft.model.entity;

import com.aquasoft.model.util.Assertions;

/**
 * Retains dimensions of spreadsheet grid.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public final class SpreadsheetDimens {

    private final int height;
    private final int length;

    public SpreadsheetDimens(int height, int length) {
        Assertions.assertStrictPositive(height);
        Assertions.assertStrictPositive(length);
        this.height = height;
        this.length = length;
    }

    /**
     * Returns spreadsheet's height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns spreadsheet's length.
     */
    public int getLength() {
        return length;
    }

    /**
     * Area occupied by this spreadsheet.
     * Calculated as {@code (height * length)}
     */
    public int getArea() {
        return height * length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpreadsheetDimens that = (SpreadsheetDimens) o;
        return height == that.height && length == that.length;
    }

    @Override
    public int hashCode() {
        int result = height;
        result = 31 * result + length;
        return result;
    }

    @Override
    public String toString() {
        return "[height=" + height + ", length=" + length + "]";
    }
}
