package com.aquasoft.model.entity.token;

import com.aquasoft.model.exception.TokenCreationException;
import org.apache.commons.lang3.StringUtils;

/**
 * Tokens is way to represent first pass of parsing source
 * textual math expression. For example user might print
 * expression in a form {@code A1 --> =4+2*B1} on first
 * stage this expression will be transformed onto tokens
 * representation. For this particular example 2 value tokens,
 * 2 operator tokens and one reference.
 *
 * @author Alexey Kovalev
 * @since 23.08.2015
 */
public abstract class AbstractExpressionToken<T> {

    /**
     * String representation of this token.
     */
    private final String sourceExpression;

    /**
     * Retains result of parsing.
     */
    private T parsedValue;

    /**
     * No outside instantiation. Use {@link #from(String)} factory method instead.
     *
     * @param sourceExpression - source expression which will be parsed
     * @throws TokenCreationException - in case when some exceptional condition occurred
     * during token's creation or parsing phase
     */
    protected AbstractExpressionToken(String sourceExpression) throws TokenCreationException {
        this.sourceExpression = sourceExpression;
        this.parsedValue = parse(sourceExpression);
    }

    public static AbstractExpressionToken<?> from(String stringRepresentation) throws TokenCreationException {
        stringRepresentation = StringUtils.defaultString(stringRepresentation);
        if (stringRepresentation.matches(IntValueExpressionToken.VALUE_REGEXP_MASK)) {
            return new IntValueExpressionToken(stringRepresentation);
        } else if (stringRepresentation.matches(ReferenceExpressionToken.VALUE_REGEXP_MASK)) {
            return new ReferenceExpressionToken(stringRepresentation);
        } else if (stringRepresentation.matches(TextualValueExpressionToken.VALUE_REGEXP_MASK)) {
            return new TextualValueExpressionToken(stringRepresentation);
        } else if (OperatorExpressionToken.isOperatorSign(stringRepresentation.charAt(0))) {
            return new OperatorExpressionToken(stringRepresentation);
        } else {
            throw new TokenCreationException("Could not find any concrete AbstractExpressionToken implementation " +
                    "which matches expression [" + stringRepresentation + "]");
        }
    }

    /**
     * Returns parsed value or {@code NULL} otherwise.
     */
    public final T getParsedValue() {
        return parsedValue;
    }

    /**
     * Here subclasses should implement logic required for parsing content's value.
     *
     * @param sourceExpression - expression to parse
     */
    protected abstract T parse(String sourceExpression) throws TokenCreationException;

    @SuppressWarnings("unchecked") // move responsibility to caller side
    public final <R> AbstractExpressionToken<R> castTo(Class<R> typeToken) {
        return (AbstractExpressionToken<R>) this;
    }

    public boolean isOperator() {
        return false;
    }

    public boolean isReference() {
        return false;
    }

    public String getSourceExpression() {
        return sourceExpression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbstractExpressionToken<?> that = (AbstractExpressionToken<?>) o;
        return !(parsedValue != null ? !parsedValue.equals(that.parsedValue) : that.parsedValue != null);
    }

    @Override
    public int hashCode() {
        return parsedValue != null ? parsedValue.hashCode() : 0;
    }
}
