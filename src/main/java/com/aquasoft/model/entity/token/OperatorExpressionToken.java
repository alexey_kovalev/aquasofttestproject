package com.aquasoft.model.entity.token;

import com.aquasoft.model.entity.BinaryOperation;
import com.aquasoft.model.exception.TokenCreationException;

/**
 * Represents part of the source expression which is one of the
 * arithmetic operators like ({@code + or - or * or /})
 *
 * @author Alexey Kovalev
 * @since 08.09.2015
 */
public final class OperatorExpressionToken extends AbstractExpressionToken<BinaryOperation> {

    protected OperatorExpressionToken(String stringRepresentation) throws TokenCreationException {
        super(stringRepresentation);
    }

    public static boolean isOperatorSign(char operatorSign) {
        return BinaryOperation.isValidOperator(operatorSign);
    }

    @Override
    protected BinaryOperation parse(String sourceExpression) throws TokenCreationException {
        return BinaryOperation.fromOperatorSign(sourceExpression.charAt(0));
    }

    @Override
    public boolean isOperator() {
        return true;
    }
}
