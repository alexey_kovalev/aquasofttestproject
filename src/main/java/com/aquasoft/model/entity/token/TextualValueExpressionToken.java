package com.aquasoft.model.entity.token;

import com.aquasoft.model.exception.TokenCreationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents part of the source expression which contains
 * textual data which starts from ({@code '}) symbol.
 *
 * @author Alexey Kovalev
 * @since 08.09.2015
 */
public final class TextualValueExpressionToken extends AbstractExpressionToken<String> {

    public static final String VALUE_REGEXP_MASK = "^'(.*)$";

    protected TextualValueExpressionToken(String stringRepresentation) throws TokenCreationException {
        super(stringRepresentation);
    }

    @Override
    protected String parse(String sourceExpression) throws TokenCreationException {
        Matcher matcher = Pattern.compile(VALUE_REGEXP_MASK).matcher(sourceExpression);
        if (matcher.matches()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }
}
