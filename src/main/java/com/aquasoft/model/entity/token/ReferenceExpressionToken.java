package com.aquasoft.model.entity.token;

import com.aquasoft.model.entity.cell.CellCoordinate;
import com.aquasoft.model.exception.TokenCreationException;
import com.aquasoft.model.util.CellUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents part of the source expression which refers to another cell
 * in the spreadsheet.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public final class ReferenceExpressionToken extends AbstractExpressionToken<CellCoordinate> {

    public static final String VALUE_REGEXP_MASK = "([A-Z]+)(\\d+)";

    protected ReferenceExpressionToken(String stringRepresentation) throws TokenCreationException {
        super(stringRepresentation);
    }

    @Override
    protected CellCoordinate parse(String sourceExpression) throws TokenCreationException {
        Matcher matcher = Pattern.compile(VALUE_REGEXP_MASK).matcher(sourceExpression);
        if (matcher.matches()) {
            final int columnIndex = CellUtils.getColumnIndexForLetter(matcher.group(1).charAt(0));
            final int rowIndex = Integer.valueOf(matcher.group(2));
            return new CellCoordinate(rowIndex, columnIndex);
        } else {
            return null;
        }
    }

    @Override
    public boolean isReference() {
        return true;
    }
}
