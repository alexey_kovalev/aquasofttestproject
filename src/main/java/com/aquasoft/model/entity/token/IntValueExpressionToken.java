package com.aquasoft.model.entity.token;

import com.aquasoft.model.exception.TokenCreationException;

/**
 * Represents part of the source expression which consists of only
 * from positive integer number.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public final class IntValueExpressionToken extends AbstractExpressionToken<Integer> {

    public static final String VALUE_REGEXP_MASK = "\\d+";

    protected IntValueExpressionToken(String stringRepresentation) throws TokenCreationException {
        super(stringRepresentation);
    }

    @Override
    protected Integer parse(String sourceExpression) throws TokenCreationException {
        return Integer.valueOf(sourceExpression);
    }
}
