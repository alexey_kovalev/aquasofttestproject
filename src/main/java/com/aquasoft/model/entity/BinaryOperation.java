package com.aquasoft.model.entity;

import com.aquasoft.model.entity.cell.ComputationResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents binary arithmetic operations which might be
 * performed between two operands.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public enum BinaryOperation {

    Add('+') {
        @Override
        public ComputationResult apply(ComputationResult operandA, ComputationResult operandB) {
            return ComputationResult.ofInt(operandA.getResultAsInt() + operandB.getResultAsInt());
        }
    },

    Sub('-') {
        @Override
        public ComputationResult apply(ComputationResult operandA, ComputationResult operandB) {
            return ComputationResult.ofInt(operandA.getResultAsInt() - operandB.getResultAsInt());
        }
    },

    Mul('*') {
        @Override
        public ComputationResult apply(ComputationResult operandA, ComputationResult operandB) {
            return ComputationResult.ofInt(operandA.getResultAsInt() * operandB.getResultAsInt());
        }
    },

    Div('/') {
        @Override
        public ComputationResult apply(ComputationResult operandA, ComputationResult operandB) {
            return ComputationResult.ofInt(operandA.getResultAsInt() / operandB.getResultAsInt());
        }
    };

    /**
     * Used to enhance speed of finding operation by its operator sign representation;
     */
    private static final Map<Character, BinaryOperation> operatorSignToOperationMapping = new HashMap<>();

    static {
        for (BinaryOperation operation : BinaryOperation.values()) {
            operatorSignToOperationMapping.put(operation.operatorSign, operation);
        }
    }

    private final Character operatorSign;

    BinaryOperation(Character operatorSign) {
        this.operatorSign = operatorSign;
    }

    /**
     * Static factory which returns instance of {@link BinaryOperation}
     * from its string operator representation or {@code NULL} otherwise.
     * <p/>
     * Before invoking this method you have to make sure that operation
     * exists for given operator sign using {@link #isValidOperator}
     * method invokation.
     */
    public static BinaryOperation fromOperatorSign(Character operatorSign) {
        return operatorSignToOperationMapping.get(operatorSign);
    }

    public static boolean isValidOperator(Character operatorSign) {
        return operatorSignToOperationMapping.get(operatorSign) != null;
    }

    public static List<Character> getOperatorsList() {
        List<Character> res = new ArrayList<>();
        for (BinaryOperation operation : BinaryOperation.values()) {
            res.add(operation.getOperatorSign());
        }
        return res;
    }

    public abstract ComputationResult apply(ComputationResult operandA, ComputationResult operandB);

    public Character getOperatorSign() {
        return operatorSign;
    }

    @Override
    public String toString() {
        return operatorSign.toString();
    }
}
