package com.aquasoft.model.entity;

import com.aquasoft.model.entity.cell.Cell;
import com.aquasoft.model.entity.cell.CellType;
import com.aquasoft.model.entity.token.AbstractExpressionToken;
import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Encapsulates parsed spreadsheet data and provides
 * facilities for adding spreadsheet header and
 * parsed cell's instances.
 *
 * @author Alexey Kovalev
 * @since 14.09.2015
 */
public final class Spreadsheet {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    private final SpreadsheetDimens spreadsheetDimens;
    private final Set<Cell> cells;
    private final CellCoordinateGenerator cellCoordinateGenerator;

    public Spreadsheet(SpreadsheetDimens spreadsheetDimens) {
        this.spreadsheetDimens = spreadsheetDimens;
        this.cells = new TreeSet<>(Cell.BY_CELL_COORDINATE_COMPARATOR);
        this.cellCoordinateGenerator = CellCoordinateGenerator.of(spreadsheetDimens);
    }

    public void addNewCellByExpression(CellType cellType, List<AbstractExpressionToken<?>> postfixExpression) {
        cells.add(Cell.fromInfixExpression(cellType, cellCoordinateGenerator.nextCoordinate(), postfixExpression));
    }

    public SpreadsheetDimens getSpreadsheetDimens() {
        return spreadsheetDimens;
    }

    public Set<Cell> getCells() {
        return Collections.unmodifiableSet(cells);
    }

    public boolean isFull() {
        return !cellCoordinateGenerator.canGenerateNext();
    }
}
