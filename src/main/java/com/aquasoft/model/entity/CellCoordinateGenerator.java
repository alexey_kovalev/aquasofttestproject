package com.aquasoft.model.entity;

import com.aquasoft.model.entity.cell.CellCoordinate;

/**
 * Assists in generating cell's coordinates for
 * given spreadsheet dimensions.
 * <p/>
 * Internally implemented around the assumption that
 * data received per row basis - i.e. row contains
 * list of cells (traversing will be done in horizontal direction).
 *
 * @see SpreadsheetDimens
 * @author Alexey Kovalev
 * @since 13.09.2015
 */
public final class CellCoordinateGenerator {

    public static final int START_COLUMN_INDEX = 1;
    public static final int START_ROW_INDEX = 1;

    private final SpreadsheetDimens spreadsheetDimens;
    private final int maxRow;
    private final int maxColumn;

    private int currentRow = START_ROW_INDEX;
    private int currentColumn = START_COLUMN_INDEX;

    /**
     * No outside instantiation. Use
     * factory methods instead.
     */
    private CellCoordinateGenerator(SpreadsheetDimens spreadsheetDimens) {
        this.spreadsheetDimens = spreadsheetDimens;
        this.maxRow = spreadsheetDimens.getHeight();
        this.maxColumn = spreadsheetDimens.getLength();
    }

    public static CellCoordinateGenerator of(SpreadsheetDimens spreadsheetDimens) {
        return new CellCoordinateGenerator(spreadsheetDimens);
    }

    /**
     * Generates next cell's coordinate.
     *
     * @throws IndexOutOfBoundsException in case when spreadsheet size has been exhausted
     * and can't generate next cell's coordinate
     */
    public CellCoordinate nextCoordinate() {
        if (currentRow > maxRow || currentColumn > maxColumn) {
            throw new IndexOutOfBoundsException("Can't generate next cell's coordinate. " +
                    "Spreadsheet size " + spreadsheetDimens + "] exhausted.");
        }

        final CellCoordinate res;

        res = new CellCoordinate(currentRow, currentColumn);
        ++currentColumn;
        if (currentColumn > maxColumn) {
            currentColumn = START_COLUMN_INDEX;
            currentRow++;
        }

        return res;
    }

    public boolean canGenerateNext() {
        return currentRow < maxRow || (currentRow == maxRow && currentColumn <= maxColumn);
    }
}
