package com.aquasoft.model.entity.cell;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Contains result of computation for particular cell.
 * <p/>
 * Keep immutability of this class.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public class ComputationResult {

    public static final String EMPTY_RES = StringUtils.EMPTY;
    public static final String ERROR_MESSAGE_TEXT = "#";
    public static final ComputationResult NOT_DONE_COMPUTATION = ofText(EMPTY_RES);

    private final String result;

    private ComputationResult(String result) {
        this.result = result;
    }

    public static ComputationResult ofInt(int result) {
        return ofText(String.valueOf(result));
    }

    public static ComputationResult ofError() {
        return ofText(ERROR_MESSAGE_TEXT);
    }

    public static ComputationResult ofText(String resultText) {
        return new ComputationResult(resultText);
    }

    public boolean isComputed() {
        // potentially might be replaced with "!=" instead of "equals()" due to the class's immutability
        return !NOT_DONE_COMPUTATION.equals(this);
    }

    public int getResultAsInt() {
        return Integer.valueOf(result);
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(result);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ComputationResult that = (ComputationResult) o;
        return Objects.equals(result, that.result);

    }

    @Override
    public int hashCode() {
        return result != null ? result.hashCode() : 0;
    }
}
