package com.aquasoft.model.entity.cell;

import com.aquasoft.model.entity.BinaryOperation;
import com.aquasoft.model.entity.token.AbstractExpressionToken;
import com.aquasoft.model.exception.Exceptions;
import com.aquasoft.model.util.CellUtils;
import org.apache.commons.collections4.ListUtils;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents one cell's data of the spreadsheet.
 *
 * @author Alexey Kovalev
 * @since 23.08.2015
 */
public final class Cell {

    public static final Comparator<Cell> BY_CELL_COORDINATE_COMPARATOR =
            (o1, o2) -> o1.getCoordinate().compareTo(o2.getCoordinate());

    /**
     * Defines cell's coordinate (position) in spreadsheet grid.
     */
    private final CellCoordinate coordinate;

    /**
     * Contains cell's type.
     * @see CellType
     */
    private final CellType cellType;

    /**
     * Contains first pass of parsing cell's math expression.
     * @see AbstractExpressionToken
     */
    private final List<AbstractExpressionToken<?>> parsedPostfixExpression;

    /**
     * Set of cells from which this cell depends on.
     */
    private final Set<CellCoordinate> dependencies;

    /**
     * Retains result of computation for this cell.
     */
    private ComputationResult computationResult;

    private Cell(CellType cellType, CellCoordinate coordinate, List<AbstractExpressionToken<?>> parsedPostfixExpression) {
        Exceptions.throwIfNull(coordinate, "You have to pass NOT NULL coordinate parameter for cell instance construction.");
        Exceptions.throwIfNull(parsedPostfixExpression, "You have to pass NOT NULL parsedPostfixExpression parameter for cell instance construction.");
        this.cellType = cellType;
        this.coordinate = coordinate;
        this.parsedPostfixExpression = parsedPostfixExpression;
        this.dependencies = createDependenciesSet(parsedPostfixExpression);
        this.computationResult = ComputationResult.NOT_DONE_COMPUTATION;
    }

    public static Cell fromInfixExpression(CellType cellType, CellCoordinate coordinate, List<AbstractExpressionToken<?>> infixExpression) {
        return new Cell(cellType, coordinate, CellUtils.infixToPostfixNotation(infixExpression));
    }

    private Set<CellCoordinate> createDependenciesSet(List<AbstractExpressionToken<?>> parsedPostfixExpression) {
        return parsedPostfixExpression
                .stream()
                .filter(token -> token.isReference())
                .map(referenceToken -> referenceToken.castTo(CellCoordinate.class).getParsedValue())
                .collect(Collectors.toSet());
    }

    /**
     * Calculates cell's expression.
     *
     * @param computedDependenciesPool - map of already calculated cells onto which this cell
     *                                   might refer
     */
    public void calculate(Map<CellCoordinate, Cell> computedDependenciesPool) {
        if (cellType == CellType.Expression) {
            Deque<ComputationResult> stack = new ArrayDeque<>();
            for (AbstractExpressionToken<?> token : parsedPostfixExpression) {
                if (token.isOperator()) {
                    AbstractExpressionToken<BinaryOperation> operator = token.castTo(BinaryOperation.class);
                    ComputationResult operand1 = stack.pop();
                    ComputationResult operand2 = stack.pop();
                    ComputationResult res = operator.getParsedValue().apply(operand2, operand1);
                    stack.push(res);
                } else { // operand
                    stack.push(getResult(token, computedDependenciesPool));
                }
            }
            computationResult = stack.pop();
        } else {
            computationResult = getResultFromFirstToken();
        }
    }

    private ComputationResult getResult(AbstractExpressionToken<?> token, Map<CellCoordinate, Cell> computedDependenciesPool) {
        if (token.isReference()) {
            CellCoordinate referenceTo = token.castTo(CellCoordinate.class).getParsedValue();
            return computedDependenciesPool.get(referenceTo).getComputationResult();
        } else {
            return ComputationResult.ofText(token.getParsedValue().toString());
        }
    }

    private ComputationResult getResultFromFirstToken() {
        AbstractExpressionToken<?> firstToken = !parsedPostfixExpression.isEmpty() ? parsedPostfixExpression.get(0) : null;
        return firstToken == null || firstToken.getParsedValue() == null ?
                ComputationResult.ofError() :
                ComputationResult.ofText(firstToken.getParsedValue().toString());
    }

    /**
     * Returns {@code true} when cell's content has been calculated
     * and {@code false} otherwise.
     */
    public boolean isCalculated() {
        return computationResult.isComputed();
    }

    /**
     * Returns set of dependencies for this cell.
     * <p/>
     * This method returns safe wrapper of dependencies set.
     * So the caller side is not allowed to invoke mutator methods on it
     * otherwise it ends up with exception.
     */
    public Set<CellCoordinate> getDependencies() {
        return Collections.unmodifiableSet(dependencies);
    }

    public boolean isDependenciesFree() {
        return dependencies.isEmpty();
    }

    public CellCoordinate getCoordinate() {
        return coordinate;
    }

    public ComputationResult getComputationResult() {
        return computationResult;
    }

    public List<AbstractExpressionToken<?>> getParsedPostfixExpression() {
        return Collections.unmodifiableList(parsedPostfixExpression);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Cell that = (Cell) o;
        return coordinate.equals(that.coordinate) && parsedPostfixExpression.equals(that.parsedPostfixExpression);
    }

    @Override
    public int hashCode() {
        int result = coordinate.hashCode();
        result = 31 * result + ListUtils.hashCodeForList(parsedPostfixExpression);
        return result;
    }
}
