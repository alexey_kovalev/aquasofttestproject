package com.aquasoft.model.entity.cell;

import com.aquasoft.model.util.Assertions;

/**
 * Defines Cell's coordinate (position) into
 * the grid (spreadsheet).
 * <p/>
 * Keep immutability of this class. So it's prohibited
 * and now it's not possible to change internal state
 * of created instance during its lifecycle.
 *
 * @author Alexey Kovalev
 * @since 23.08.2015
 */
public final class CellCoordinate implements Comparable<CellCoordinate> {

    /**
     * Row index where Cell is positioned.
     * Indexing starts from 1.
     */
    private final int rowIndex;

    /**
     * Column index where Cell is positioned.
     * Indexing starts from 1.
     */
    private final int columnIndex;

    public CellCoordinate(int rowIndex, int columnIndex) {
        Assertions.assertStrictPositive(rowIndex);
        Assertions.assertStrictPositive(columnIndex);
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    @Override
    public int compareTo(CellCoordinate that) {
        if (rowIndex != that.rowIndex) {
            return rowIndex - that.rowIndex;
        }
        // Row indexes equal - then estimate column indexes
        return columnIndex - that.columnIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CellCoordinate that = (CellCoordinate) o;
        return rowIndex == that.rowIndex && columnIndex == that.columnIndex;
    }

    @Override
    public int hashCode() {
        int result = rowIndex;
        result = 31 * result + columnIndex;
        return result;
    }

    @Override
    public String toString() {
        return String.format("[row=%s; column=%s]", rowIndex, columnIndex);
    }
}
