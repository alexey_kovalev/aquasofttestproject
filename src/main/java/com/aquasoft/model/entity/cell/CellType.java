package com.aquasoft.model.entity.cell;

import org.apache.commons.lang3.StringUtils;

/**
 * Contains all possible cell's types.
 *
 * @author Alexey Kovalev
 * @since 13.09.2015.
 */
public enum CellType {

    PositiveNumber, Text, Expression, Empty, NotDefined;

    public static final String POSITIVE_NUMBER_REGEXP_MASK = "\\d+";
    public static final String TEXT_REGEXP_MASK = "^'.*$";
    public static final String EXPRESSION_REGEXP_MASK = "^=.*$";

    public static CellType from(String cellExpression) {
        if (StringUtils.isBlank(cellExpression)) {
            return Empty;
        }

        cellExpression = StringUtils.defaultString(cellExpression);
        if (cellExpression.matches(POSITIVE_NUMBER_REGEXP_MASK)) {
            return PositiveNumber;
        } else if (cellExpression.matches(TEXT_REGEXP_MASK)) {
            return Text;
        } else if (cellExpression.matches(EXPRESSION_REGEXP_MASK)) {
            return Expression;
        } else {
            return NotDefined;
        }
    }
}
