package com.aquasoft.model.entity;

import com.aquasoft.model.entity.cell.Cell;
import com.aquasoft.model.entity.cell.CellCoordinate;
import com.aquasoft.model.exception.Exceptions;

import java.util.Map;

/**
 * Contains result of spreadsheet computation
 * along with source data.
 *
 * @author Alexey Kovalev
 * @since 17.09.2015.
 */
public final class SpreadsheetComputationResultWrapper {

    private final Map<CellCoordinate, Cell> result;
    private final Spreadsheet sourceSpreadsheet;

    public SpreadsheetComputationResultWrapper(Map<CellCoordinate, Cell> result, Spreadsheet sourceSpreadsheet) {
        Exceptions.throwIfNull(result, "You have to supply NOT NULL result parameter.");
        Exceptions.throwIfNull(sourceSpreadsheet, "You have to supply NOT NULL sourceSpreadsheet parameter.");
        this.result = result;
        this.sourceSpreadsheet = sourceSpreadsheet;
    }

    public Map<CellCoordinate, Cell> getResultMap() {
        return result;
    }

    public Spreadsheet getSourceSpreadsheet() {
        return sourceSpreadsheet;
    }

    public int getSpreadsheetHeight() {
        return sourceSpreadsheet.getSpreadsheetDimens().getHeight();
    }

    public int getSpreadsheetLength() {
        return sourceSpreadsheet.getSpreadsheetDimens().getLength();
    }
}
