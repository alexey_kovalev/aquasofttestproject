package com.aquasoft.model.parser;

import java.util.function.Function;

/**
 * Defines transformation which will be performed upon
 * source math expression.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public interface ITransformation extends Function<String, String> {
}
