package com.aquasoft.model.parser;

import com.aquasoft.model.entity.cell.CellType;
import com.aquasoft.model.entity.token.AbstractExpressionToken;
import com.aquasoft.model.exception.Exceptions;
import com.aquasoft.model.exception.SyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Decorator around {@link ISyntaxParser} implementation
 * intended to perform sequence of accumulated transformations
 * upon source expression before passing source expression
 * to wrapped parser.
 *
 * @author Alexey Kovalev
 * @since 13.09.2015.
 */
public class TransformationsDecoratedParser implements ISyntaxParser {

    private final ISyntaxParser wrappedParser;

    private final TransformationsHolder transformationsHolder;

    /**
     * No outside instantiations.
     * Use {@link #builder(ISyntaxParser)} instead.
     */
    private TransformationsDecoratedParser(ISyntaxParser wrappedParser, TransformationsHolder transformationsHolder) {
        this.wrappedParser = wrappedParser;
        this.transformationsHolder = transformationsHolder;
    }

    public static Builder builder(ISyntaxParser parserToWrap) {
        return new Builder(parserToWrap);
    }

    /**
     * Simplified version for creating instance of {@link SimpleParser}
     * without any transformation rules.
     * <p/>
     * Might be useful for unit testing.
     */
    public static TransformationsDecoratedParser create(ISyntaxParser parserToWrap) {
        return builder(parserToWrap).build();
    }

    @Override
    public List<AbstractExpressionToken<?>> parse(String cellSourceExpression) throws SyntaxException {
        String expressionToParse = transformationsHolder.applyByCellType(
                cellSourceExpression,
                CellType.from(cellSourceExpression)
        );
        return wrappedParser.parse(expressionToParse);
    }

    public static final class Builder {

        private final ISyntaxParser parserToWrap;
        private final TransformationsHolder transformationsHolder;

        private Builder(ISyntaxParser parserToWrap) {
            this.parserToWrap = parserToWrap;
            this.transformationsHolder = new TransformationsHolder();
        }

        public Builder addTransformationByCellType(CellType cellType, ITransformation transformation) {
            validate(cellType, transformation);
            transformationsHolder.addForCellType(cellType, transformation);
            return this;
        }

        private void validate(CellType cellType, ITransformation transformation) {
            Exceptions.throwIfNull(transformation, "You have to specify NOT NULL transformation");
            if (cellType != CellType.Expression) {
                Exceptions.notImplementedFeatureYet("For now transformations applicable only for \"Expression\" cell type.");
            }
        }

        public TransformationsDecoratedParser build() {
            return new TransformationsDecoratedParser(parserToWrap, transformationsHolder);
        }
    }

    /**
     * Defines a helper holder which contains list of transformations
     * which will be sequentially applied upon source math expression
     * depending on cell's type.
     */
    private static class TransformationsHolder {
        final Map<CellType, List<ITransformation>> transformationsMap = new HashMap<>();

        public void addForCellType(CellType cellType, ITransformation transformation) {
            List<ITransformation> transformationsByType = transformationsMap.get(cellType);
            if (transformationsByType == null) {
                transformationsByType = new ArrayList<>();
                transformationsMap.put(cellType, transformationsByType);
            }
            transformationsByType.add(transformation);
        }

        /**
         * Applies accumulated transformations to source expression according to
         * cell's type.
         */
        public String applyByCellType(String cellSourceExpression, CellType cellType) {
            List<ITransformation> transformationsByType = transformationsMap.get(cellType);
            String transformedResult = cellSourceExpression;
            if (transformationsByType != null) {
                for (ITransformation transformation : transformationsByType) {
                    transformedResult = transformation.apply(transformedResult);
                }
            }
            return transformedResult;
        }
    }
}
