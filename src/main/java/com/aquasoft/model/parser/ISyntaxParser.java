package com.aquasoft.model.parser;

import com.aquasoft.model.entity.token.AbstractExpressionToken;
import com.aquasoft.model.exception.SyntaxException;

import java.util.List;

/**
 * Concrete implementations have to be stateless entities.
 * Parsing has to be performed in function-style approach.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public interface ISyntaxParser {

    List<AbstractExpressionToken<?>> parse(String cellSourceExpression) throws SyntaxException;
}
