package com.aquasoft.model.parser;

import com.aquasoft.model.entity.BinaryOperation;
import com.aquasoft.model.entity.cell.CellType;
import com.aquasoft.model.entity.token.AbstractExpressionToken;
import com.aquasoft.model.exception.SyntaxException;
import com.aquasoft.model.exception.TokenCreationException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link ISyntaxParser} based on set of 
 * sequential transformation which will be sequentially applied
 * to source math expression.
 * <p/>
 * This class implements some kind of {@code Chain of responsibility}
 * pattern.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public final class SimpleParser implements ISyntaxParser {

    private static final char[] operators;

    static {
        List<Character> operatorsList = BinaryOperation.getOperatorsList();
        operators = new char[operatorsList.size()];
        for (int i = 0; i < operatorsList.size(); i++) {
            operators[i] = operatorsList.get(i);
        }
    }

    private static int findOperatorOccurrenceIndex(String expression) {
        return StringUtils.indexOfAny(expression, operators);
    }

    @Override
    public List<AbstractExpressionToken<?>> parse(String cellSourceExpression) throws SyntaxException {
        try {
            List<AbstractExpressionToken<?>> result = new ArrayList<>();
            // for not negative integers or textual cell's content
            final CellType cellType = CellType.from(cellSourceExpression);
            if (cellType == CellType.Text || cellType == CellType.PositiveNumber) {
                result.add(AbstractExpressionToken.from(cellSourceExpression));
                return result;
            } else if (cellType == CellType.Expression) {
                String withoutEqualSign = StringUtils.substring(cellSourceExpression, 1);
                return toTokens(withoutEqualSign);
            }
            return result;
        } catch (TokenCreationException e) {
            throw new SyntaxException("Exception occurred during syntax parsing of source " +
                    "expression [" + cellSourceExpression + "]. Caused by ", e);
        }
    }

    private List<AbstractExpressionToken<?>> toTokens(String sourceExpression) throws TokenCreationException {
        List<AbstractExpressionToken<?>> result = new ArrayList<>();
        int occurrenceIndex;
        while ((occurrenceIndex = findOperatorOccurrenceIndex(sourceExpression)) != -1) {
            final int indexAfterOperatorOccurrence = occurrenceIndex + 1;
            String expressionBeforeOperator = StringUtils.substring(sourceExpression, 0, occurrenceIndex);
            result.add(AbstractExpressionToken.from(expressionBeforeOperator));
            String operator = StringUtils.substring(sourceExpression, occurrenceIndex, indexAfterOperatorOccurrence);
            result.add(AbstractExpressionToken.from(operator));
            sourceExpression = StringUtils.substring(sourceExpression, indexAfterOperatorOccurrence);
        }
        result.add(AbstractExpressionToken.from(sourceExpression));
        return result;
    }
}
