package com.aquasoft.model.parser;

import org.apache.commons.lang3.StringUtils;

/**
 * Contains a list of useful transformation algorithms.
 *
 * @author Alexey Kovalev
 * @since 25.08.2015
 */
public final class Transformations {

    private Transformations() {
        throw new AssertionError("No instances of Transformations");
    }

    public static ITransformation pruneWhitespace() {
        return StringUtils::deleteWhitespace;
    }

    public static ITransformation toUpperCase() {
        return StringUtils::upperCase;
    }
}
