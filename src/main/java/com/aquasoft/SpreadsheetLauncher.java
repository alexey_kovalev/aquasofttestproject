package com.aquasoft;

import com.aquasoft.controller.ISpreadsheetController;
import com.aquasoft.controller.SpreadsheetControllerImpl;
import com.aquasoft.view.ConsoleSpreadsheetView;

import java.io.IOException;

/**
 * Main entry point for launching spreadsheet
 * processor application.
 *
 * @author Alexey Kovalev
 * @since 23.08.2015
 */
public class SpreadsheetLauncher {

    public static void main(String[] args) throws IOException {
        ISpreadsheetController controller = new SpreadsheetControllerImpl();
        ConsoleSpreadsheetView consoleView = new ConsoleSpreadsheetView(controller);
        controller.setSpreadsheetView(consoleView);
        consoleView.interceptStdInput(args);
    }
}
