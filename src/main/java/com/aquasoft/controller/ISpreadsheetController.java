package com.aquasoft.controller;

import com.aquasoft.view.ISpreadsheetView;

/**
 * This interface should be implemented by classes
 * which are going to play role of {@code Controller}
 * according to {@code MVC} architecture.
 *
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public interface ISpreadsheetController {

    void setSpreadsheetView(ISpreadsheetView spreadsheetView);

    void onSpreadsheetHeaderReceived(String spreadsheetHeader);

    void onNextCellDataReceived(String infixCellExpression);
}
