package com.aquasoft.controller;

import com.aquasoft.Constants;
import com.aquasoft.model.entity.Spreadsheet;
import com.aquasoft.model.entity.SpreadsheetComputationResultWrapper;
import com.aquasoft.model.entity.SpreadsheetDimens;
import com.aquasoft.model.entity.cell.Cell;
import com.aquasoft.model.entity.cell.CellCoordinate;
import com.aquasoft.model.entity.cell.CellType;
import com.aquasoft.model.exception.CircularDependencyException;
import com.aquasoft.model.exception.SyntaxException;
import com.aquasoft.model.exception.ValidationException;
import com.aquasoft.model.parser.ISyntaxParser;
import com.aquasoft.model.parser.SimpleParser;
import com.aquasoft.model.parser.Transformations;
import com.aquasoft.model.parser.TransformationsDecoratedParser;
import com.aquasoft.model.processor.CellsProcessor;
import com.aquasoft.model.util.log.ILogger;
import com.aquasoft.model.util.log.LoggerFactory;
import com.aquasoft.model.validation.SourceExpressionValidationRules;
import com.aquasoft.model.validation.cell.ValidationRulesHolder;
import com.aquasoft.model.validation.validator.AbstractValidator;
import com.aquasoft.view.ISpreadsheetView;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @see ISpreadsheetController
 * @author Alexey Kovalev
 * @since 24.08.2015
 */
public class SpreadsheetControllerImpl implements ISpreadsheetController {

    private static final ILogger logger = LoggerFactory.getStdOutBasedLogger();

    private final AbstractValidator<Cell> cellValidator;
    private final AbstractValidator<String> sourceExpressionValidator;
    private final ISyntaxParser syntaxParser;
    private Spreadsheet currentSpreadsheet;
    private ISpreadsheetView spreadsheetView;

    public SpreadsheetControllerImpl() {
        syntaxParser = createSyntaxParser();
        sourceExpressionValidator = createSourceExpressionValidator();
        cellValidator = createCellValidator();
    }

    private ISyntaxParser createSyntaxParser() {
        return TransformationsDecoratedParser
                .builder(new SimpleParser())
                .addTransformationByCellType(CellType.Expression, Transformations.pruneWhitespace())
                .addTransformationByCellType(CellType.Expression, Transformations.toUpperCase())
                .build();
    }

    private AbstractValidator<String> createSourceExpressionValidator() {
        ValidationRulesHolder<String> validationRulesHolder = ValidationRulesHolder.<String>builder()
                .addRule(SourceExpressionValidationRules.notEmptyExpression())
                .addRule(SourceExpressionValidationRules.noOperatorSignAtBeginning())
                .addRule(SourceExpressionValidationRules.noOperatorSignAtTheEnd())
                .build();
        return AbstractValidator.withRules(validationRulesHolder.rulesToList());
    }

    private AbstractValidator<Cell> createCellValidator() {
        ValidationRulesHolder<Cell> validationRulesHolder = ValidationRulesHolder.<Cell>builder()
//                .addRule()
//                .addRule()
                .build();
        return AbstractValidator.withRules(validationRulesHolder.rulesToList());
    }

    @Override
    public void setSpreadsheetView(ISpreadsheetView spreadsheetView) {
        this.spreadsheetView = spreadsheetView;
    }

    @Override
    public void onSpreadsheetHeaderReceived(String spreadsheetHeader) {
        logger.debug("Controller: onSpreadsheetHeaderReceived data -> header");
        String[] headerData = spreadsheetHeader.split(Constants.CELL_DATA_DELIMITER);
        final int height = Integer.valueOf(headerData[0]);
        final int length = Integer.valueOf(headerData[1]);
        logger.debug(String.format("Controller: height [%s], length [%s]", height, length));
        currentSpreadsheet = new Spreadsheet(new SpreadsheetDimens(height, length));
    }

    @Override
    public void onNextCellDataReceived(String infixCellExpression) {
        logger.debug("Controller: onNextCellDataReceived() expression [" + infixCellExpression + "]");
        try {
            sourceExpressionValidator.validate(infixCellExpression);
            currentSpreadsheet.addNewCellByExpression(CellType.from(infixCellExpression), syntaxParser.parse(infixCellExpression));
            if (currentSpreadsheet.isFull()) {
                logger.debug("Controller: Spreadsheet is filled. Start graph evaluation.");
                Map<CellCoordinate, Cell> res = CellsProcessor.from(currentSpreadsheet).evaluate();

                if (spreadsheetView != null) {
                    spreadsheetView.onResult(new SpreadsheetComputationResultWrapper(res, currentSpreadsheet));
                }
            }
        } catch (ValidationException | SyntaxException | CircularDependencyException e) {
            notifyViewOnError(null, e);
        }
    }

    private void notifyViewOnError(String message, Exception ex) {
        if (spreadsheetView != null) {
            spreadsheetView.onError(StringUtils.defaultString(message), ex);
        }
    }
}
